#!/bin/bash

set -eu

Main()
{
  ##############################################################################
  # INPUT
  ##############################################################################

    local git_branch="${1? Missing git branch from where we want to install Elixir !!!}"


  ##############################################################################
  # VARS
  ##############################################################################

    local branch="${git_branch#v*}"

    local first_character="${git_branch:0:1}"

    printf "\nFIRST CHARACTER: ${first_character}\n"

    case ${first_character} in
      [0-9] )
        local git_branch="v${git_branch}"
      ;;
    esac


  ##############################################################################
  # CONSTANTS
  ##############################################################################

    PHOENIX_FOLDER_NAME=phoenix-"${branch}"

    PHOENIX_DOWNLOAD_URL=https://github.com/phoenixframework/phoenix/archive/${git_branch}.zip


  ##############################################################################
  # EXECUTION
  ##############################################################################

    printf "\nPHOENIX DOWNLOAD URL: ${PHOENIX_DOWNLOAD_URL}\n"
    printf "\nPHOENIX FOLDER NAME: ${PHOENIX_FOLDER_NAME}\n"
    printf "\nGIT BRANCH: ${git_branch}\n"

    # installs the package manager
    mix local.hex --force

    # installs rebar and rebar3
    mix local.rebar --force

    if [ ! -f "${HOME}"/bin/rebar ]; then
      ln -s "${HOME}"/.mix/rebar "${HOME}"/bin/rebar
    fi

    if [ ! -f "${HOME}"/bin/rebar3 ]; then
      ln -s "${HOME}"/.mix/rebar3 "${HOME}"/bin/rebar3
    fi

    # DOWNLOAD PHOENIX FROM A BRANCH OR TAG IN GITHUB
    curl -fsSL -o phoenix.zip "${PHOENIX_DOWNLOAD_URL}"
    unzip phoenix.zip
    rm -rf phoenix.zip

    # COMPILE AND INSTALL PHOENIX FROM SOURCE
    cd "${PHOENIX_FOLDER_NAME}"/installer
    MIX_ENV=prod mix do archive.build, archive.install --force

    # CLEANUP
    cd -
    rm -rf "${PHOENIX_FOLDER_NAME}"
}

Main "${@}"
