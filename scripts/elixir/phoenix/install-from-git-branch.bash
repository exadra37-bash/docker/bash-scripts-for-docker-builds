#!/bin/bash

set -eu

Main()
{
  ##############################################################################
  # INPUT
  ##############################################################################

    local git_branch="${1? Missing git branch from where we want to install Elixir !!!}"


  ##############################################################################
  # VARS
  ##############################################################################

    local first_character="${git_branch:0:1}"

    printf "\nFIRST CHARACTER: ${first_character}\n"

    case ${first_character} in
      [0-9] )
        local git_branch="v${git_branch}"
      ;;
    esac


  ##############################################################################
  # EXECUTION
  ##############################################################################

    printf "\nGIT BRANCH: ${git_branch}\n"

    # installs the package manager
    mix local.hex --force

    # installs rebar and rebar3
    mix local.rebar --force

    if [ ! -f "${HOME}"/bin/rebar ]; then
      ln -s "${HOME}"/.mix/rebar "${HOME}"/bin/rebar
    fi

    if [ ! -f "${HOME}"/bin/rebar3 ]; then
      ln -s "${HOME}"/.mix/rebar3 "${HOME}"/bin/rebar3
    fi

    # CLONE PHOENIX FROM A BRANCH OR TAG IN GITHUB
    git clone --depth 1 --branch "${git_branch}" https://github.com/phoenixframework/phoenix.git

    # COMPILE AND INSTALL PHOENIX FROM SOURCE
    cd phoenix/installer
    MIX_ENV=prod mix do archive.build, archive.install --force

    # CLEANUP
    cd -
    rm -rf phoenix

    # SMOKE TEST
    mix phx.new --version
}

Main "${@}"
