#!/bin/bash

set -eu

Main()
{
  ##############################################################################
  # INPUT
  ##############################################################################

    local git_branch="${1? Missing git branch from where we want to install Elixir !!!}"


  ##############################################################################
  # VARS
  ##############################################################################

    local remove_make=false

    local first_character="${git_branch:0:1}"

    printf "\nFIRST CHARACTER: ${first_character}\n"

    case ${first_character} in
      [0-9] )
        local git_branch="v${git_branch}"
      ;;
    esac


  ##############################################################################
  # EXECUTION
  ##############################################################################

    printf "\nGIT BRANCH: ${git_branch}\n"

    # INSTALL DEPENDENCIES
    if ! which make; then
      # just making sure that make is installed
      apt update
      apt -y install make
      local remove_make=true
    fi

    # DOWNLOAD ELIXIR FROM A GITHUB BRANCH OR TAG
    git clone --depth 1 --branch "${git_branch}" https://github.com/elixir-lang/elixir.git

    # INSTALL ELIXIR
    cd elixir
    make clean test
    make install clean
    cd -

    # CLEANUP
    rm -rf elixir

    # Do not remove make if installed
    if [ "${remove_make}" = "true" ]; then
      apt -y auto-remove make
    fi

    # SMOKE TEST
    elixir --version
}

Main "${@}"
