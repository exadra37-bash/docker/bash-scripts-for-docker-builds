#!/bin/sh

set -eu

Main()
{
  ##############################################################################
  # INPUT
  ##############################################################################

    local user_name="${1? Missing user name !!!}"

    local user_uid="${2? Missing user UID !!!}"

    local user_shell_path="${3? Missing the shell bin path}"

    local user_bin_path="${4? Missing user bin path !!!}"


  ##############################################################################
  # EXECUTION
  ##############################################################################

    printf "\n---> CREATING A USER <---\n"
    printf "USER NAME: ${user_name}\n"
    printf "USER UID: ${user_uid}\n"
    printf "USER SHELL PATH: ${user_shell_path}\n"
    printf "USER BIN PATH: ${user_bin_path}\n"

    useradd -m -u "${user_uid}" -s "${user_shell_path}" "${user_name}"

    su "${user_name}" -c "sh -c 'mkdir -p ${user_bin_path}'"
}

Main "${@}"
