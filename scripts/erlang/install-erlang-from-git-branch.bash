#!/bin/bash

set -eu

Main()
{
  ##############################################################################
  # INPUT
  ##############################################################################

    local git_branch="${1? Missing git branch or tag from where we want to install Erlang !!!}"


  ##############################################################################
  # VARS
  ##############################################################################

    local branch="${git_branch#OTP-*}"

    local first_character="${git_branch:0:1}"

    printf "\nGIT BRANCH FIRST CHARACTER: ${first_character}\n"

    case ${first_character} in
      [0-9] )
        local git_branch="OTP-${git_branch}"
      ;;
    esac


  ##############################################################################
  # CONSTANTS
  ##############################################################################

    # TODO:
    #   * some of the dependencies are in the build-essential package, thus we
    #     need to check the ones that already exist, so that we will only remove
    #     the ones we installed.

    # $ apt install build-essential
    # Reading package lists... Done
    # Building dependency tree
    # Reading state information... Done
    # The following additional packages will be installed:
    #   binutils cpp cpp-6 dpkg-dev fakeroot g++ g++-6 gcc gcc-6 libalgorithm-diff-perl libalgorithm-diff-xs-perl libalgorithm-merge-perl libasan3
    #   libatomic1 libcc1-0 libcilkrts5 libdpkg-perl libfakeroot libfile-fcntllock-perl libgcc-6-dev libgomp1 libisl15 libitm1
    #   liblocale-gettext-perl liblsan0 libmpc3 libmpfr4 libmpx2 libquadmath0 libstdc++-6-dev libtsan0 libubsan0 make
    # Suggested packages:
    #   binutils-doc cpp-doc gcc-6-locales debian-keyring g++-multilib g++-6-multilib gcc-6-doc libstdc++6-6-dbg gcc-multilib manpages-dev
    #   autoconf automake libtool flex bison gdb gcc-doc gcc-6-multilib libgcc1-dbg libgomp1-dbg libitm1-dbg libatomic1-dbg libasan3-dbg
    #   liblsan0-dbg libtsan0-dbg libubsan0-dbg libcilkrts5-dbg libmpx2-dbg libquadmath0-dbg libstdc++-6-doc make-doc
    # The following NEW packages will be installed:
    #   binutils build-essential cpp cpp-6 dpkg-dev fakeroot g++ g++-6 gcc gcc-6 libalgorithm-diff-perl libalgorithm-diff-xs-perl
    #   libalgorithm-merge-perl libasan3 libatomic1 libcc1-0 libcilkrts5 libdpkg-perl libfakeroot libfile-fcntllock-perl libgcc-6-dev libgomp1
    #   libisl15 libitm1 liblocale-gettext-perl liblsan0 libmpc3 libmpfr4 libmpx2 libquadmath0 libstdc++-6-dev libtsan0 libubsan0 make
    # 0 upgraded, 34 newly installed, 0 to remove and 0 not upgraded.
    # Need to get 33.8 MB of archives.
    # After this operation, 139 MB of additional disk space will be used.

    local INSTALL_BUILD_DEPENDENCIES="
      autoconf
      dpkg-dev
      gcc
      g++
      make
      libncurses5-dev
      unixodbc-dev
      libssl-dev
      libsctp-dev
      libwxgtk3.0-dev
      default-jdk
      fop
      libxml2-utils
      xsltproc
    "

    local REMOVE_BUILD_DEPENDENCIES="
      autoconf
      libncurses5-dev
      unixodbc-dev
      libssl-dev
      libsctp-dev
      libwxgtk3.0-dev
      default-jdk
      fop
      libxml2-utils
      xsltproc
    "

    local RUNTIME_DEPENDENCIES="
      libcanberra-gtk-module
      procps
      libncurses5
      libwxbase3.0-0v5
      libwxgtk3.0-0v5
      libodbc1
      libssl1.1
      libsctp1
      man
    "

  ##############################################################################
  # EXECUTION
  ##############################################################################

    printf "\nGIT BRANCH: ${git_branch}\n"

    # INSTALL DEPENDENCIES
    apt update
    apt -y -q install --no-install-recommends ${INSTALL_BUILD_DEPENDENCIES} ${RUNTIME_DEPENDENCIES}

    # DOWNLOAD ERLANG FROM A GITHUB BRANCH OR TAG
    git clone --depth 1 --branch "${git_branch}" https://github.com/erlang/otp.git OTP

    # INSTALL ERLANG
    cd OTP
    export ERL_TOP="${PWD}"
    ./otp_build autoconf
    gnuArch="$(dpkg-architecture --query DEB_BUILD_GNU_TYPE)"
    ./configure --build="$gnuArch"
    make -j$(nproc)
    make install
    cd -
    rm -rf OTP

    # SYSTEM CLEANUP
    # we don't need them examples and doc folders, and to see docs just use like:
    #   $ erl -man ets
    find /usr/local -name examples | xargs rm -rf
    find /usr/local -name doc | xargs rm -rf
    apt -y auto-remove ${REMOVE_BUILD_DEPENDENCIES}

    # SMOKE TEST
    erl -eval 'erlang:display(erlang:system_info(otp_release)), halt().'  -noshell
    erl -eval 'io:fwrite("~1p~n", [lists:sort(erlang:loaded())]), halt().'  -noshell

    # To list all erlang modules installed:
    # erl> rp(lists:sort(erlang:loaded())).
}

Main "${@}"
