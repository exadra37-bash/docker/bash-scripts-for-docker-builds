#!/bin/sh

set -eu

Main()
{
  ##############################################################################
  # INPUT
  ##############################################################################

    local git_branch="${1? Missing git branch or tag from where we want to install Rebar3 !!!}"


  ##############################################################################
  # EXECUTION
  ##############################################################################

    printf "\nGIT BRANCH: ${git_branch}\n"

    # DOWNLOAD REBAR3 FROM A GITHUB BRANCH OR TAG
    git clone --depth 1 --branch "${git_branch}" https://github.com/erlang/rebar3.git

    # INSTALL REBAR3
    cd rebar3
    ./bootstrap
    mv rebar3 ~/bin
    cd -
    rm -rf rebar3
}

Main "${@}"
