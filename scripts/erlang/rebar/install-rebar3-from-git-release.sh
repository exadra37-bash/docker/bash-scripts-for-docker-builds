#!/bin/sh

  printf "\nREBAR3_DOWNLOAD_URL: ${REBAR3_DOWNLOAD_URL}\n" && \
  curl -fsSL -o rebar3.zip "${REBAR3_DOWNLOAD_URL}" && \
  unzip rebar3.zip && \
  rm -f rebar3.zip && \
  cd rebar3-"${DOCKER_REBAR3_VERSION}" && \
  ./bootstrap && \
  mv rebar3 ~/bin && \
  cd - && \
  rm -rf rebar3-"${DOCKER_REBAR3_VERSION}"
