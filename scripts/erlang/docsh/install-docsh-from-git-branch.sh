#!/bin/sh
# @link https://github.com/erszcz/docsh

set -eu

Main()
{
  ##############################################################################
  # INPUT
  ##############################################################################

    local git_branch="${1? Missing git branch or tag from where we want to install DOCSH !!!}"


  ##############################################################################
  # EXECUTION
  ##############################################################################

    printf "\nGIT BRANCH: ${git_branch}\n"

    # DOWNLOAD DOCSH FROM A GITHUB BRANCH OR TAG
    git clone --depth 1 --branch "${git_branch}" https://github.com/erszcz/docsh

    # INSTALL DOCSH
    cd docsh
    echo "y" | ./install.sh
}

Main "${@}"
